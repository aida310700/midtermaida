package kz.aitu.demo.service;

import kz.aitu.demo.model.Person;
import kz.aitu.demo.repository.PersonRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    private final PersonRepository personRepository;

    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }

    public Optional<Person> getPersonById(long id){
        return personRepository.findById(id);
    }

    public void deleteById(long id){
        personRepository.deleteById(id);
    }


     public Person updatePerson(@RequestBody Person person){
        return personRepository.save(person);

    }}
