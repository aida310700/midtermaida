package kz.aitu.demo.controller;

import kz.aitu.demo.model.Person;
import kz.aitu.demo.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private PersonService personService;
    public PersonController(PersonService personService) {
        this.personService = personService;
    }
    @GetMapping("/api/person")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personService.getAll());
    }
    @GetMapping("/api/person/{id}")
    public ResponseEntity getPersonById(@PathVariable int id){
        return ResponseEntity.ok(personService.getPersonById(id));
    }
    @DeleteMapping("/api/deletePerson/{id}")
    public void deletePerson(@PathVariable long id){
        personService.deleteById(id);
    }

    @PutMapping(path = "/api/person")
    public ResponseEntity<?>updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.updatePerson(person));
    }

}
